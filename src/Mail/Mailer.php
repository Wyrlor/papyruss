<?php

/**
 *  File: SMTPMail.php
 *  Created on: 6/20/16
 *
 *  @author Joseph Gengarella <joseph.gengarella@gmail.com>
 *
 *  Allows for simple sending of mail using SMTP for AWS
 *
 *  Required Libraries
 *  Mail.php
 *  Mail/mime.php
 */

namespace Papyruss\Mail;

use Mail;
use Mail_mime;
use PEAR;

class Mailer
{

    public $crlf;
    public $errors;
    public $mailType    = 'smtp';
    public $contentType = 'multipart/html';

    protected $settings;
    protected $headers;
    protected $mime_boundary;
    protected $body         = '';
    protected $textBody     = '';
    protected $mimeParams   = [
        'eol' => '\r\n',
        'head_charset' => 'UTF-8',
        'text_charset' => 'UTF-8',
        'html_charset' => 'UTF-8'
    ];

    /**
     * Mailer constructor.
     *
     * @param bool $auth
     * @param string $crlf - EOL for the email client. Default for SMTP is \r\n, may need to change to \n
     *      for other clients such as 'mail'
     */
    public function __construct($auth = true, $crlf = "\r\n")
    {
        $this->settings = [
            'host'      => EMAIL_HOST,
            'post'      => EMAIL_PORT,
            'username'  => EMAIL_USERNAME,
            'password'  => EMAIL_PASSWORD,
            'auth'      => $auth,
        ];

        $this->crlf = $crlf;
        $this->mimeParams['eol'] = $this->crlf;
    }

    /**
     *  Set the mail client to SMTP - this setting is enabled by default
     */
    public function isSMTP()
    {
        $this->mailType = 'smtp';
    }

    /**
     *  Set the mail client to 'mail'
     *      As of 4/21 this is not fully supported
     *
     * @TODO Add support for mail client.
     */
    public function isMail()
    {
        $this->mailType = 'mail';
    }

    /**
     * Construct the headers for the email. Supports multiple recipients, reply to, cc, and bcc addresses.
     *      As of 4/21 content types other than the default ('multipart/html') are not supported.
     *
     * Required fields: To, From, and Subject.
     *
     * Errors sent back through $this->errors. If empty no errors are present.
     *
     * @param $data
     *      ['to']       - ['Recipient 1' => 'recipient1@email.com', 'Recipient 2' => 'recipient2@email.com']
     *      ['from']     - ['Sender Name' => 'sender@email.com']
     *      ['reply-to'] - ['Reply 1' => 'reply1@email.com', 'Reply 2' => 'reply2@email.com']
     *      ['cc']       - ['CC Name' => 'cc@email.com']
     *      ['bcc']      - ['BCC Name' => 'bcc@email.com']
     *      ['subject']  - ['Email Subject']
     * @return $this
     */
    public function setHeaders($data)
    {
        $this->setRecipients($data);
        $this->setFrom($data);
        $this->setReplyTo($data);
        $this->setCC($data);
        $this->setBCC($data);
        $this->setSubject($data);
        $this->setContentType();

        return $this;
    }

    /**
     * Add one or more addresses to the specified field in the $headers array
     *
     * @param $data
     * @param $field
     * @param bool $required
     * @return $this|string
     */
    private function addAddress($data, $field, $required = false)
    {
        $address = $this->headers[$field] ?? '';
        $hasError = false;

        if (isset($data[$field])) {
            foreach ($data[$field] as $email => $name) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    if (!empty($address)) {
                        $address .= ', ';
                    }
                    $address .= ($name ?? '') . ' <' . $email . '>';
                } else {
                    $hasError = true;
                }
            }
        } else {
            $hasError = true;
        }

        if ($hasError && $required === true) {
            $this->errors[$address] = 'Invalid email in the ' . $field . ' field';
            return $this;
        }

        return $this->headers[$field] = mb_encode_mimeheader($address);
    }

    /**
     * Set and format the 'from' email address. Will only grab the first item of the array.
     *
     * @param $data
     *      ['from'] => ['Sender Name' => 'sender@email.com']
     * @return $this|string
     */
    private function setFrom($data)
    {
        $from = '';
        $hasError = false;

        if (isset($data['from']) && count($data['from']) === 1) {
            foreach ($data['from'] as $email => $name) {
                if (filter_var($email, FILTER_VALIDATE_EMAIL) && empty($from)) {
                    $from = ($name ?? '') . ' <' . $email . '>';
                } else  {
                    $hasError = true;
                }
            }
        } else {
            $hasError = true;
        }

        if ($hasError) {
            $this->errors['from'] = 'Invalid email in the from field';
            return $this;
        }

        return $this->headers['from'] =  mb_encode_mimeheader($from);
    }

    /**
     * Set the Content Type of the email. If this is called with no data set the type to it's default type.
     *
     * @param array $data
     * @return mixed|string
     *
     * @TODO: Add support for all content types.
     */
    public function setContentType($data = [])
    {
        if (isset($data['content-type'])) {
            $type = $data['content-type'];
        } else {
            $type = 'multipart/alternative';
        }

        return $this->contentType = $type;
    }

    /**
     * @param $data
     *      ['subject'] => ['Email Subject']
     * @return $this|string
     */
    public function setSubject($data)
    {
        $subject = '';
        $hasError = false;

        if (isset($data['subject'])) {
            foreach ($data['subject'] as $data_subject) {
                $subject = $data_subject;
            }
        } else {
            $hasError = true;
        }

        if ($hasError) {
            $this->errors['subject'] = 'A subject must be entered';
            return $this;
        }

        return $this->headers['subject'] = mb_encode_mimeheader($subject);
    }

    /**
     * Set the HTML content for the email body and set a basic Text version by simply stripping out the HTML
     * tags from the HTML content. This will be a basic default in case no Text body is sent
     *
     * @param $body
     * @param bool $isFile
     *      - If isFile is true, the $body will hold the file path of the email content.
     */
    public function setBody($body, $isFile = false)
    {
        if ($isFile) {
            $this->body = file_get_contents($body);
            $this->textBody = strip_tags(file_get_contents($body));
        } else {
            $body = utf8_encode($body);

            $this->body = $body;
            $this->textBody = strip_tags($body);
        }
    }

    /**
     * Set the Text content for the email.
     *
     * @param $body
     * @param bool $isFile
     *      - If isFile is true, the $body will hold the file path of the email content.
     */
    public function setTextBody($body, $isFile = false)
    {
        if ($isFile) {
            $this->textBody = strip_tags(file_get_contents($body));
        } else {
            $body = utf8_encode($body);

            $this->textBody = strip_tags($body);
        }
    }

    /**
     * If properly set up, this will send the email based on the settings.
     *
     * @return array
     */
    public function send()
    {
        $mime = new Mail_mime($this->crlf);

        $mime->setContentType($this->contentType);
        $mime->setTXTBody($this->textBody);
        $mime->setHTMLBody($this->body);

        $recipients = $this->headers['to'];
        $body       = $mime->get($this->mimeParams);
        $headers    = $mime->headers($this->headers, false);

        $mail   = Mail::factory($this->mailType, $this->settings);

        $result = $mail->send($recipients, $headers, $body);

        if (PEAR::isError($result)) {
            return [
                'status'    => 'failure',
                'message'   => $result->getMessage()
            ];
        } else {
            return [
                'status'    => 'success',
                'message'   => 'Message successfully sent'
            ];
        }
    }

    /**
     * Generate a mime boundary
     *
     * @return string
     */
    public function generateBoundary()
    {
        return '"_' . bin2hex(openssl_random_pseudo_bytes(12)) . '"';
    }

    /**
     * Getters and Setters for the address related fields.
     *
     * @param $data
     * @return Mailer|string
     */

    public function addRecipients($data)
    {
        return $this->addAddress($data, 'to');
    }

    private function setRecipients($data)
    {
        return $this->addAddress($data, 'to', true);
    }

    public function addReplyTo($data)
    {
        return $this->addAddress($data, 'reply-to');
    }

    private function setReplyTo($data)
    {
        return $this->addAddress($data, 'reply-to');
    }

    public function addCC($data)
    {
        return $this->addAddress($data, 'cc');
    }

    private function setCC($data)
    {
        return $this->addAddress($data, 'cc');
    }

    public function addBCC($data)
    {
        return $this->addAddress($data, 'bcc');
    }

    private function setBCC($data)
    {
        return $this->addAddress($data, 'bcc');
    }
}
