<?php
/**
 * File: Router.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@gmail.com>
 *
 * Routes URLs and makes them more readable for the user
 *
 * How to use:
 * $router = new Router();
 * $router->add('path/to/something', 'ControllerName@method')
 * $router->add('using/{url}/{params}', 'ControllerName@method')
 *      Params are sent as an array:
 *          using/10/123 returns
 *          Array("url" => 10, "params" => 123)
 */
namespace Papyruss\Routing;

class Router
{
    private $routes = [];
    private $not_found;
    private $prefix = 'App\\Controllers\\';

    public function __construct()
    {
        $this->not_found = function ($url)
        {
            echo '404 ' . $url . ' was not found';
        };
    }

    /**
     * @param $url
     * @param $action
     *
     * Add the route to the routing array
     */
    public function add($url, $action)
    {
        $this->routes[$url] = $action;
    }

    /**
     * @param $action
     *
     * Set a url not found if that uri is entered
     */
    public function setNotFound($action)
    {
        $this->not_found = $action;
    }

    /**
     * @return bool
     *
     * Deploy the router on the front facing pages
     */
    public function deploy()
    {
        // Loop through the routers array
        foreach($this->routes as $url => $action)
        {
            // Parse the value in the array for params and match them with the uri
            $params = self::parseRoute($url, $_SERVER['REQUEST_URI']);

            // If there is a route in the array that matches
            if ($params || $url === $_SERVER['REQUEST_URI'])
            {
                $action = explode('@', $action);
                list($controller, $method) = $action;

                $controller = $this->prefix . $controller;
                $controller = new $controller;

                return $controller->$method($params);
            }
        }
        call_user_func_array($this->not_found,[$_SERVER['REQUEST_URI']]);
        return false;
    }

    /**
     * @param $url
     * @param $current_uri
     * @return bool|string
     *
     * Get the url parameters from url and pack it into an array
     */
    public static function parseRoute($url, $current_uri)
    {
        $route = explode('/', $url);
        $uri = explode('/', $current_uri);
        $params = [];

        foreach ($uri as $index => $value)
        {
            if (!empty($value) && isset($route[$index])) {
                if (substr($route[$index], 0, 1) === '{')
                {
                    $params[str_replace(['{', '}'], '', $route[$index])] = $value;
                } else if ($value != $route[$index]) {
                    return false;
                }
            }
        }
        return $params;
    }
}