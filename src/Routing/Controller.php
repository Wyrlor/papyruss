<?php
/**
 * File: Controller.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@gmail.com>
 *
 * Basic instructions for the controller classes
 */
namespace Papyruss\Routing;

use Papyruss\Database\Model;
use Papyruss\Session\SecureSession;

class Controller
{
    public $isAuth;
    public $isLogged;

    public function __construct()
    {
        $this->isLogged = false;
        $this->isAuth   = false;
    }

    /**
     * @param $destination
     *
     * @return bool
     * Redirects to a target location
     */
    public function redirect($destination)
    {
        if (empty($destination)) {
            return true;
        }

        switch ($destination) {
            default:
                session_write_close();
                header('Location: ' . $destination);
                exit();
        }
    }

    /**
     * @param $view
     * @param array $data
     *
     * Shows a view
     *
     * @return mixed
     */
    public function show($view, $data = [])
    {
        foreach ($data as $key => $val) {
            $$key = $val;
        }

        return include(BASEPATH . $view);
    }

    /**
     * @param string $message
     * @param string $action
     * @param string $method
     * @param SecureSession $session
     *
     * Sets a success message session and displays is when called.
     *
     * Usage:
     *  Set a success message:
     *      $this->message('set', 'success', 'This is the success/error message');
     *  Get a success message:
     *      $this->message('get', 'success');
     */
    public function message($action, $method, $message, SecureSession $session)
    {
        if ($action === 'set') {
            $session->set($method, $message);
        } else if ($action === 'get') {
            $message = $session->get($method);
            if (isset($message) && !empty($message)) {
                echo '<div class="' . $method . '-message">';
                echo '<span>' . $message . '</span>';
                echo '</div>';
                $session->delete($method);
            }
        }
    }

    /**
     * @param $session
     * @param string $destination
     * @return bool
     *
     * Authentication check for controllers and views
     */
    public function isLoggedIn(SecureSession $session, $destination = '/index')
    {
        if ($session->get('user_id')) {
            $this->redirect($destination);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $check
     * @param string $value
     * @param SecureSession $session
     * @return $this
     *
     * Begin the auth checks for if a user is logged in.
     *
     * Usage:
     *  If a user is logged in as an admin user type, stay on this page, if not but they are still
     *  logged in, go to /client and if they are logged out go to /index
     *  $this->authCheck('admin')->in()->isLogged()->in('/client')->out('/index');
     *
     */
    public function authCheck(SecureSession $session, $check = 'user_id', $value = null)
    {
        if ($check === 'admin') {
            $check = 'user_type';
            $value = 1;
        } else if ($check === 'client') {
            $check = 'user_type';
            $value = 2;
        }

        $session_check =  $session->get($check) ?? null;

        if (isset($value) && ! empty($value)) {
            if (isset($session) && $session->get($check) === $value) {
                $this->isAuth = true;
            }
        } else {
            if (isset($session_check) && ! empty($session_check)) {
                $this->isAuth = true;
            }
        }

        return $this;
    }

    /**
     * @param string $check
     * @param SecureSession $session
     * @return $this
     *
     * Check to see if a session is active, if not there is no user logged in
     */
    public function isLogged(SecureSession $session, $check = 'user_id')
    {
        $this->isLogged = false;
        if (isset($session) && $session->get($check) !== '') {
            $this->isLogged = true;
        }

        return $this;
    }

    /**
     * @param string $destination
     * @return $this
     *
     * If the account being authenticated is logged in, forward to $destination
     */
    public function in($destination = '')
    {
        if (! empty($this->isAuth) && $this->isAuth === true && $destination !== '') {
            $this->redirect($destination);
        }

        return $this;
    }

    /**
     * @param string $destination
     * @return $this
     *
     * If the account being authenticated is logged out, continue with authentication
     */
    public function out($destination = '')
    {
        if ($this->isAuth !== true && $this->isLogged !== true) {
            $this->redirect($destination);
        }

        return $this;
    }

    /**
     * @param $form_entries
     * @param $errors
     * @param string $postInfo
     * @param SecureSession $session
     * @param Model $db
     * @return array Validates form data and returns the parsed form data and errors.
     *
     * Validates form data and returns the parsed form data and errors.
     *
     * Example:
     * $errors = [];
     * $post = [
     * 'field_name' => ['Readable Error Name' => 'required|email|min:3']
     * ]
     *
     * $this->validation($post, $errors)
     */
    public function validation($form_entries, &$errors, SecureSession $session = null, Model $db = null, $postInfo = '')
    {
        $post = [];

        if ($postInfo === '') {
            $postInfo = $_POST;
        }

        foreach ($form_entries as $form_name => $entry) {
            $readable_name = key($entry);

            if (!empty($entry[key($entry)])) {
                $conditions = explode('|', $entry[key($entry)]);
            } else {
                $conditions = [];
            }

            if ($conditions) {
                if (in_array('array', $conditions)) {
                    foreach ($postInfo[$form_name] as $key => $val) {
                        $post[$form_name][$key] = htmlspecialchars($val);
                    }
                } else {
                    $post[$form_name] = htmlspecialchars($postInfo[$form_name]);
                }

                /*
                 * Current checks:
                 *      Minimum length, maximum length, and required
                 */
                $this->checkLength($conditions, $post, $form_name, $readable_name, $errors);

                /*
                 * Current checks:
                 *      Email address, integer, floating point, alphanumeric, or equal to another field
                 */
                $this->checkValidType($conditions, $post, $form_name, $readable_name, $errors, $postInfo);

                /*
                 * Current checks:
                 *      CSRF
                 */
                $this->checkSecurity($conditions, $post, $form_name, $errors, $session);

                /*
                 * Current checks:
                 *      Unique field
                 */
                $this->checkDB($conditions, $post, $form_name, $readable_name, $errors, $db);

            } else {
                if (isset($postInfo[$form_name])) {
                    $post[$form_name] = htmlspecialchars($postInfo[$form_name]);
                }
            }
        }
        return $post;
    }

    /**
     * @param $conditions
     * @param $post
     * @param $form_name
     * @param $readable_name
     * @param $errors
     *
     * Currently checks for:
     *  Minimum length, maximum length, and required
     */
    private function checkLength($conditions, &$post, $form_name, $readable_name, &$errors)
    {
        $min_length = array_search_partial('min', $conditions);
        if (isset($min_length)) {
            $min = explode(':', $conditions[$min_length]);
            if (strlen($post[$form_name]) < $min[1]) {
                $errors[$form_name] = $readable_name . ' must be longer than ' . $min[1] . ' characters';
            }
        } else if (in_array('required', $conditions)) {
            if (empty($post[$form_name])) {
                $errors[$form_name] = $readable_name . ' was left empty';
            }
        }

        $max_length = array_search_partial('max', $conditions);
        if (isset($max_length)) {
            $max = explode(':', $conditions[$max_length]);
            if (strlen($post[$form_name]) > $max[1]) {
                $errors[$form_name] = $readable_name . ' must be shorter than ' . $max[1] . ' characters';
            }
        }

        return;
    }

    /**
     * @param $conditions
     * @param $post
     * @param $form_name
     * @param $readable_name
     * @param $errors
     * @param $postInfo
     *
     * Currently checks for:
     *  Email address, integer, floating point, alphanumeric, or equal to another field
     */
    private function checkValidType($conditions, &$post, $form_name, $readable_name, &$errors, $postInfo)
    {
        if (in_array('email', $conditions)) {
            if (!filter_var($post[$form_name], FILTER_VALIDATE_EMAIL)) {
                $errors[$form_name] = $readable_name . ' must be a valid email address';
            }
        }

        if (in_array('int', $conditions)) {
            if (!is_int($post[$form_name])) {
                $errors[$form_name] = $readable_name . ' must be a whole number';
            }
        }

        if (in_array('float', $conditions)) {
            if (!is_float($post[$form_name])) {
                $errors[$form_name] = $readable_name . ' must be a number';
            }
        }

        if (in_array('alpha', $conditions)) {
            if (!ctype_alnum($post[$form_name])) {
                $errors[$form_name] = $readable_name . ' must be a number or a letter';
            }
        }

        $equal_to = array_search_partial('equals', $conditions);
        if (isset($equal_to)) {
            $target = explode(':', $conditions[$equal_to]);
            $target = htmlspecialchars($postInfo[$target[1]]);
            if ($post[$form_name] !== $target) {
                $errors[$form_name] = $readable_name . ' does not match';
            }
        }

        return;
    }

    /**
     * @param $conditions
     * @param $post
     * @param $form_name
     * @param SecureSession $session
     * @param $errors
     *
     * Currently checks for:
     *  CSRF
     */
    private function checkSecurity($conditions, &$post, $form_name, &$errors, SecureSession $session = null)
    {
        if (in_array('csrf', $conditions)) {
            if ($post[$form_name] !== $session->get('_csrf_token')) {
                $errors[$form_name] = 'An unexpected error has occurred, please try again';
            }
        }

        return;
    }

    /**
     * @param $conditions
     * @param $post
     * @param $form_name
     * @param Model $db
     * @param $readable_name
     * @param $errors
     *
     * Currently checks for:
     *  Unique field
     */
    private function checkDB($conditions, &$post, $form_name, $readable_name, &$errors, Model $db = null)
    {
        $unique_check = array_search_partial('unique', $conditions);
        if (isset($unique_check)) {
            $target = explode(':', $conditions[$unique_check]);
            $table = $target[1];
            $where = $target[2] . '= :' . $target[2];
            $bind = [':' . $target[2] => $post[$form_name]];

            $res = $db->select($table, $where, $bind, 'id');

            if ($res) {
                $errors[$form_name] = $readable_name . ' must be unique';
            }
        }

        return;
    }
}
