<?php
/**
 * File: Model.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@gmail.com>
 *
 * @update 1.0.6 Added the touch() function to allow quick updates to updated_at fields
 *
 * Implementation of PDOutil.php for this framework setup
 * @author Richard Soares <richard.soares@rxinsider.com>
 *
 * ** For PDOutil.php **
 * @internal the debug() method imports "PDOutil_error.css" for nice error styling. Adjust path to this file accordingly.
 * @update 1.0.3 added error_log() within the __construct() to record connection failures.
 * @update 1.0.4 clarified method comments with regards to calling methods.
 * @update 1.0.5 updated run() function to return last record id on insert, and affected records for update or delete.
 */
namespace Papyruss\Database;

use PDO;
use PDOException;

class Model extends PDO
{
    private $error;
    private $sql;
    private $bind;
    private $errorCallbackFunction;
    private $errorMsgFormat;

    public function __construct()
    {
        $options = [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION
        ];

        try {
            parent::__construct(DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_DATABASE . '', DB_USER, DB_PASSWORD, $options);
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            error_log('PDOutil _construct error: ' . $this->error, 0);
        }
    }

    /**
     * @param $sql
     * @param string $bind
     * @return array|bool|int|string
     *
     * This method is used to run free-form SQL statements that can't be handled by the included delete, insert, select,
     * or update methods. If no SQL errors are produced, this method will return the number of affected rows for
     * DELETE, INSERT, and UPDATE statements, or an object of results for SELECT, DESCRIBE, and PRAGMA statements.
     *
     */
    public function run($sql, $bind = '')
    {
        $this->sql   = trim($sql);
        $this->bind  = (! is_array($bind)) ? [$bind] : $bind;
        $this->error = '';

        $return = false;

        try {
            $pdostmt = $this->prepare($this->sql);
            if ($pdostmt->execute($this->bind) !== false)
            {
                if (contains(strtolower($this->sql), ['select', 'describe', 'pragma'], 1)) {
                    $return = $pdostmt->fetchAll(PDO::FETCH_ASSOC);
                } else if (contains(strtolower($this->sql), ['update', 'delete'], 1)) {
                    $return = $pdostmt->rowCount();
                } else if (contains(strtolower($this->sql), ['insert'], 1)) {
                    $return = $this->lastInsertId();
                }
            }
        } catch (PDOException $e) {
            $this->error = $e->getMessage();
            $this->debug();
        }

        return $return;
    }

    /**
     * @param $table
     * @param string $where
     * @param string $bind
     * @param string $fields
     * @return bool|int
     *
     * Example #1
     * $results = $this->select("mytable");
     *
     * Example #2
     * $results = $this->select("mytable", "Gender = 'male'");
     *
     * Example #3 w/Prepared Statement
     * $search = "J";
     * $bind = array(
     *      ":search" => "%$search"
     * );
     * $results = $this->select("mytable", "FName LIKE :search", $bind);
     *
     */
    public function select($table, $where = "", $bind = "", $fields = "*")
    {
        $sql = "SELECT " . $fields . " FROM " . $table;
        if (!empty($where)) {
            $sql .= " WHERE " . $where;
        }
        $sql .= ";";

        $data = $this->run($sql, $bind);

        if (count($data) > 1) {
            return $data; // return full index of records.
        } else if (count($data) == 1){
            return $data[0]; // return single record.
        } else {
            return '';
        }

    }

    /**
     * @param $table
     * @param $info
     * @return bool|int
     *
     * If no SQL errors are produced, this method will return the number of rows affected by the INSERT statement.
     *
     * Example #1:
     * $insert = array(
     *      "FName" => "John",
     *      "LName" => "Doe",
     *      "Age" => 26,
     *      "Gender" => "male"
     * );
     * $this->insert("mytable", $insert);
     */
    public function insert($table, $info)
    {
        $fields = $this->filter($table, $info);
        $sql    = "INSERT INTO " . $table . " (" . implode($fields, ", ") . ") VALUES (:" . implode($fields, ", :") . ");";
        $bind   = array();
        foreach ($fields as $field) {
            $bind[":$field"] = $info[$field];
        }

        return $this->run($sql, $bind);
    }

    /**
     * @param $table
     * @param $info
     * @param $where
     * @param string $bind
     * @return array|bool|int|string
     *
     * If no SQL errors are produced, this method will return the number of rows affected by the UPDATE statement.
     *
     * @return bool|int
     *
     * Example #1
     * $update = array(
     *      "FName" => "Jane",
     *      "Gender" => "female"
     * );
     * $this->db->update("mytable", $update, "FName = 'John'");
     *
     * Example #2 w/Prepared Statement
     * $update = array(
     *      "id" => 24
     * );
     * $fname = "Jane";
     * $lname = "Doe";
     * $bind = array(
     *      ":fname" => $fname,
     *      ":lname" => $lname
     * );
     * $this->update("mytable", $update, "FName = :fname AND LName = :lname", $bind);
     */
    public function update($table, $info, $where, $bind = "")
    {
        $fields    = $this->filter($table, $info);
        $fieldSize = sizeof($fields);

        $sql = "UPDATE " . $table . " SET ";
        for ($f = 0; $f < $fieldSize; ++$f) {
            if ($f > 0)
                $sql .= ", ";
            $sql .= $fields[$f] . " = :" . $fields[$f];
        }
        $sql .= " WHERE " . $where . ";";

        $bind  = (! is_array($bind)) ? [$bind] : $bind;

        return $this->run($sql, $bind);
    }

    /**
     * @param $table
     * @param $where
     * @param string $bind
     *
     * If no SQL errors are produced, this method will return the number of rows affected by the DELETE statement.
     *
     * Method #1
     * $this->delete("mytable", "Age < 30");
     *
     * Method #2 w/Prepared Statement
     * $lname = "Doe";
     * $bind = array(
     *      ":lname" => $lname
     * )
     * $this->delete("mytable", "LName = :lname", $bind);
     */
    public function delete($table, $where, $bind = "")
    {
        $sql = "DELETE FROM " . $table . " WHERE " . $where . ";";
        $this->run($sql, $bind);
    }

    /**
     * @param $table
     * @param $id
     * @param string $field
     *
     * "Touches" a table row and updated the created_at field
     *
     * Example:
     * $this->touch("mytable", primary_id);
     */
    public function touch($table, $id, $field = "created_at") {
        $update[$field] = date('Y-m-d G:i:s');
        $this->update($table, $update, 'id = ' . $id);
    }

    /**
     * @param $table
     * @param $info
     * @return array
     *
     * Automatic table binding for MySQL and SQLite
     */
    private function filter($table, $info)
    {
        $driver = $this->getAttribute(PDO::ATTR_DRIVER_NAME);
        if ($driver == 'sqlite') {
            $sql = "PRAGMA table_info('" . $table . "');";
            $key = "name";
        } elseif ($driver == 'mysql') {
            $sql = "DESCRIBE " . $table . ";";
            $key = "Field";
        } else {
            $sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '" . $table . "';";
            $key = "column_name";
        }
        if (false !== ($list = $this->run($sql))) {
            foreach ($list as $record) {
                $fields[] = $record[$key];
            }

            return array_values(array_intersect($fields, array_keys($info)));
        }

        return array();
    }

    /**
     * @param $errorCallbackFunction
     * @param string $errorMsgFormat
     *
     * The error message can then be displayed, emailed, etc within the callback function.
     *
     * Example:
     *
     * function myErrorHandler($error) {
     * }
     *
     * $db = new db("mysql:host=127.0.0.1;port=0000;dbname=mydb", "dbuser", "dbpasswd");
     * $this->setErrorCallbackFunction("myErrorHandler");
     *
     * Text Version
     * $this->setErrorCallbackFunction("myErrorHandler", "text");
     *
     * Internal/Built-In PHP Function
     * $this->setErrorCallbackFunction("echo");
     *
     * @param $errorCallbackFunction
     * @param string $errorMsgFormat
     */
    public function setErrorCallbackFunction($errorCallbackFunction, $errorMsgFormat = "html")
    {
        if (in_array(strtolower($errorCallbackFunction), array("echo", "print"))) {
            $errorCallbackFunction = "print_r";
        }

        if (method_exists($this, $errorCallbackFunction)) {
            $this->errorCallbackFunction = $errorCallbackFunction;
            if (!in_array(strtolower($errorMsgFormat), array("html", "text"))) {
                $errorMsgFormat = "html";
            }
            $this->errorMsgFormat = $errorMsgFormat;
        }
    }

    /**
     * Debug the PDO statement
     */
    private function debug()
    {
        // If no other error handler is defined, then use this.
        if (!empty($this->errorCallbackFunction)) {

            $error = array("Error" => $this->error);
            if (!empty($this->sql))
                $error["SQL Statement"] = $this->sql;
            if (!empty($this->bind))
                $error["Bind Parameters"] = trim(print_r($this->bind, true));

            $backtrace = debug_backtrace();
            if (!empty($backtrace)) {
                foreach ($backtrace as $info) {
                    if ($info["file"] != __FILE__)
                        $error["Backtrace"] = $info["file"] . " at line " . $info["line"];
                }
            }

            $msg = "";
            if ($this->errorMsgFormat == "html") {
                if (!empty($error["Bind Parameters"]))
                    $error["Bind Parameters"] = "<pre>" . $error["Bind Parameters"] . "</pre>";
                $css = trim(file_get_contents(dirname(__FILE__) . "/PDOutil_error.css")); // set this path
                $msg .= '<style type="text/css">' . "\n" . $css . "\n</style>";
                $msg .= "\n" . '<div class="db-error">' . "\n\t<h3>SQL Error</h3>";
                foreach ($error as $key => $val)
                    $msg .= "\n\t<label>" . $key . ":</label>" . $val;
                $msg .= "\n\t</div>\n</div>";
            } elseif ($this->errorMsgFormat == "text") {
                $msg .= "SQL Error\n" . str_repeat("-", 50);
                foreach ($error as $key => $val)
                    $msg .= "\n\n$key:\n$val";
            }

            $func = $this->errorCallbackFunction;
            $this->{$func}($msg); // neat little trick to call a variable function.
        }
    }

    /**
     * @param $msg
     *
     * Simple Callback Function.
     */
    public function basicCallbackFunction($msg) {
        print_r($msg);
    }
}