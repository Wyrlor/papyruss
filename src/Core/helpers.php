<?php
/**
 * File: functions.php
 * Created on: 7/19/16
 *
 * @author Joseph Gengarella <joseph.gengarella@gmail.com>
 *
 * Helper functions that can be used globally
 */

/**
 * @param $arr
 * @param null $kill
 * @return string
 *
 * Neatly prints out an array, also kills further execution if second parameter is set
 */
if (!function_exists('debug')) {
    function debug($arr, $kill = null, $options = [])
    {
        $css = '
        <style type="text/css">
            ._debug-wrapper {
                line-height: 16px;
            }
            ._debug-type {
                font-size: 80%;
                color: #563A89;
            }
            ._debug-number {
                color: #B8520E;
            }
            ._debug-string {
                color: #08961E;
            }
            ._debug-options {
                display: none;
            }
            ._debug-option-title:hover > ._debug-options {
                display: block;
            }
        </style>
        ';

        $debug_options = [
            'strlen' => $options['strlen'] ?? 100,
            'width' => $options['width'] ?? 25,
            'depth' => $options['depth'] ?? 10
        ];

        $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

        do {
            $caller = array_shift($backtrace);
        } while ($caller && !isset($caller['file']));

        if (isset($caller)) {
            $calling_file = $caller['file'] . ':' . $caller['line'] . "\n";
        }

        echo  $css . '<pre class="_debug-wrapper">'. $calling_file;
        print_debug_options($debug_options);
        var_debug($arr, $debug_options);
        echo '</pre>';

        if ($kill !== null) {
            die();
        }
    }
}

/**
 * @param $variable
 * @param $options
 * @param int $i
 * @param array $objects
 * @return bool|string
 *
 * Prints an array or object neatly and organized with additional type information
 */
function var_debug($variable, $options, $i = 0, &$objects = array())
{
    $search = array("\0", "\a", "\b", "\f", "\n", "\r", "\t", "\v");
    $replace = array('\0', '\a', '\b', '\f', '\n', '\r', '\t', '\v');

    $string = '';

    switch(gettype($variable)) {
        case 'boolean':
            $string.= '<span class="_debug-type">bool</span> ' . $variable ?? 'false';
            break;
        case 'integer':
            $string.= '<span class="_debug-type">int</span> <span class="_debug-number">' . $variable . '</span>';
            break;
        case 'double':
            $string.= '<span class="_debug-type">double</span> <span class="_debug-number">' . $variable . '</span>';
            break;
        case 'resource':
            $string.= '<span class="_debug-type">[resource]</span>';
            break;
        case 'NULL':
            $string.= '<span class="_debug-type">null</span> ';
            break;
        case 'unknown type':
            $string.= '<span class="_debug-type">???</span>';
            break;
        case 'string':
            $len = strlen($variable);
            $variable = str_replace($search,$replace,substr($variable,0,$options['strlen']),$count);
            $variable = substr($variable,0,$options['strlen']);
            if ($len < $options['strlen']) {
                $string.= '<span class="_debug-type">string('.$len.') </span> <span class="_debug-string">"' . $variable . '"</span>';
            } else {
                $string.= '<span class="_debug-type">string('.$len.') </span> <span class="_debug-string">"' . $variable . '"</span>...';
            }
            break;
        case 'array':
            $len = count($variable);
            if ($i === $options['depth']) {
                $string.= '<strong>array</strong> (length = '.$len.') {...}';
            } else if (! isset($len) || empty($len)) {
                $string.= '<strong>array</strong>(length = 0) {}';
            } else {
                $keys = array_keys($variable);
                $spaces = str_repeat(' ',$i * 6);
                $string .= "\n" . $spaces . "<strong>array</strong> <em>(length = $len)</em>";
                $count = 0;

                foreach($keys as $key) {
                    if ($count === $options['width']) {
                        $string .= "\n".$spaces."  ...";
                        break;
                    }
                    $string .= "\n" . $spaces . "  '$key' => ";
                    $string .= var_debug($variable[$key], $options, $i+1, $objects);
                    $count++;
                }
                $string .= "\n" . $spaces;
            }
            break;
        case 'object':
            $id = array_search($variable, $objects, true);
            if ($id !== false) {
                $string .= get_class($variable) . '#' . ($id + 1) . ' {...}';
            } else if($i === $options['depth']) {
                $string .= get_class($variable) . ' {...}';
            } else {
                $id          = array_push($objects, $variable);
                $array       = (array) $variable;
                $spaces      = str_repeat(' ', $i * 6);
                $string     .= get_class($variable) . "#$id\n" . $spaces;
                $properties  = array_keys($array);

                foreach($properties as $property) {
                    $name    = str_replace("\0",':',trim($property));
                    $string .= "\n" . $spaces . "  '$name' => ";
                    $string .= var_debug($array[$property], $options, $i + 1, $objects);
                }
                $string.= "\n" . $spaces;
            }
            break;
    }

    if ($i > 0) {
        return $string;
    }

    echo  $string;

    return true;
}

/**
 * @param $options
 *
 * Prints the options passed into the debug function
 */
function print_debug_options($options)
{
    $option_count = count($options);
    echo "\n<div class='_debug-option-title'><strong>options</strong> <em>($option_count)</em> =>\n";
    echo "<div class='_debug-options'>";
    foreach ($options as $key => $option) {
        echo "   '$key' => <span class='_debug-number '>$option</span>\n";
    }
    echo "</div></div>";
}

/**
 * @return string
 *
 * Generate a csrf token for form submission
 */
function csrf_generator()
{
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
    } else {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }
}

/**
 * @param $arr
 * @return bool
 *
 * Check an array to see if it is associative
 */
function isAssoc($arr)
{
    return array_keys($arr) === range(0, count($arr) - 1);
}

/**
 * @param $haystack
 * @param array $needles
 * @param null $limit
 * @return bool
 *
 * Checks the haystack for an item in the needle array. Limit sets how far into the haystack to search
 */
function contains($haystack, array $needles, $limit = null) {
    foreach ($needles as $needle) {
        $pos = strpos($haystack, $needle);

        if ($pos !== false) {
            if (isset($limit)) {
                return $pos <= $limit ? true : false;
            }
            return true;
        }
    }

    return false;
}

/**
 * @param $str1
 * @param $str2
 * @return bool
 *
 * Create hash_equals for older versions of php
 */
if(!function_exists('hash_equals'))
{
    function hash_equals($str1, $str2)
    {
        if(strlen($str1) != strlen($str2))
        {
            return false;
        }
        else
        {
            $res = $str1 ^ $str2;
            $ret = 0;
            for($i = strlen($res) - 1; $i >= 0; $i--)
            {
                $ret |= ord($res[$i]);
            }
            return !$ret;
        }
    }
}

/**
 * @param $path
 * @param string $page
 * @return array
 *
 * Extend the view to make includes easier. Just pass the path based on it's relation to the views folder.
 */
function extend_view($path, $page='')
{
    return include (BASEPATH . '/app/views/' .$path);
}

/**
 * @param $data
 * @param string $type
 * @param string $check
 *
 * Fill posted form data error free
 */
function fillData($data, $type="input", $check="")
{
    if (isset($_POST[$data])) {
        if (is_array($_POST[$data])) {
            if (in_array($check, $_POST[$data])) {
                echo ' checked';
            }
        } else {
            if ($type === "input") {
                if (isset($_POST[$data])) {
                    echo htmlspecialchars($_POST[$data]);
                    return;
                }
            } else if ($type === "select") {
                if (isset($_POST[$data]) && $_POST[$data] == $check) {
                    echo ' selected';
                }
            }else if ($type === "radio") {
                if (isset($_POST[$data]) && $_POST[$data] == $check) {
                    echo ' checked';
                }
            }
        }
    }
    echo '';
    return;
}

function formError($data) {
    if (isset($data)) {
        echo 'form-error';
    }
}

function array_search_partial($keyword, $array) {
    foreach($array as $index => $string) {
        if (strpos($string, $keyword) !== false) {
            return $index;
        }
    }
}

/**
 * Display all possible errors for debugging
 */
function display_all_errors()
{
    ini_set('display_errors', 'On');
    ini_set('html_errors', 0);
    error_reporting(-1);

    function ShutdownHandler()
    {
        if(@is_array($error = @error_get_last()))
        {
            return(@call_user_func_array('ErrorHandler', $error));
        };

        return(TRUE);
    };

    register_shutdown_function('ShutdownHandler');

    function ErrorHandler($type, $message, $file, $line)
    {
        $_ERRORS = [
            0x0001 => 'E_ERROR',
            0x0002 => 'E_WARNING',
            0x0004 => 'E_PARSE',
            0x0008 => 'E_NOTICE',
            0x0010 => 'E_CORE_ERROR',
            0x0020 => 'E_CORE_WARNING',
            0x0040 => 'E_COMPILE_ERROR',
            0x0080 => 'E_COMPILE_WARNING',
            0x0100 => 'E_USER_ERROR',
            0x0200 => 'E_USER_WARNING',
            0x0400 => 'E_USER_NOTICE',
            0x0800 => 'E_STRICT',
            0x1000 => 'E_RECOVERABLE_ERROR',
            0x2000 => 'E_DEPRECATED',
            0x4000 => 'E_USER_DEPRECATED'
        ];

        if(!@is_string($name = @array_search($type, @array_flip($_ERRORS))))
        {
            $name = 'E_UNKNOWN';
        };

        return(print(@sprintf("%s Error in file \xBB%s\xAB at line %d: %s\n", $name, @basename($file), $line, $message)));
    };

    $old_error_handler = set_error_handler("ErrorHandler");
}