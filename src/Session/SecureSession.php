<?php
/**
 * File: SecureSession.php
 * Created on: 7/20/16
 *
 * @author Joseph Gengarella <joseph.gengarella@gmail.com>
 * @version 1.0
 *
 * Manages sessions in a secure way. Originally based off of https://gist.github.com/eddmann/10262795 with modifications and improvements based on the project.
 *
 * How to use:
 * In your default/config file, set up the session by giving it key, setting the necessary flags, and starting the session
 *      $session = new \core\SecureSession('key');
 *      ini_set('session.save_handler', 'files');
 *      session_set_save_handler($session, true);
 *      session_save_path(BASEPATH . '/sessions');
 *      $session->start();
 *
 * To set/get a session
 *      $session->set('example', 'example 1');
 *      $session->get('example');
 *          output: example 1
 *
 * Creating a session array or json string
 *      $session->set('example.one', 'example 1');
 *      $session->set('example.two', 'example 2');
 *      $session->get('example');
 *          output: Array("one" => "example 1", "two" => "example 2")
 *      $session->set('example.two');
 *          output: example 2
 *
 * Using an array or json string
 *      $session->set('array example', $array)
 *      $session->get('array example');
 *          output: Array("one" => "example 1", "two" => "example 2")
 *
 * Ending/destroying a session
 *      $session->end();
 */
namespace Papyruss\Session;

use PDO;
use SessionHandler;

class SecureSession extends SessionHandler
{
    protected $key;
    protected $name;
    protected $cookie;
    protected $ttl;

    private $openssl_method;
    private $db;

    public function __construct($name = 'PHPSESSID', $cookie = [], $db_session = DATABASE_SESSIONS)
    {
        $this->key            = SESSION_HASH;
        $this->ttl            = SESSION_TIMEOUT;
        $this->openssl_method = OPENSSL_METHOD;
        $this->name           = $name;
        $this->cookie         = $cookie;

        if ($db_session === true) {
            $options = array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION
            );

            try {
                $this->db = new PDO(DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_DATABASE . '', DB_USER, DB_PASSWORD, $options);
            } catch (PDOException $e) {
                error_log('Error connecting to database: ' . $this->error, 0);
            }
        }

        if (!$this->isValid()) {
            $this->end();
        }

        $this->cookie += [
            'lifetime'  => $this->ttl * 60,
            'path'      => ini_get('session.cookie_path'),
            'domain'    => ini_get('session.cookie_domain'),
            'secure'    => isset($_SERVER['HTTPS']),
            'httponly'  => true
        ];

        $this->setup();
    }

    /**
     * @param $db_session
     *
     * Setup the settings for the session
     *
     */
    private function setup($db_session = DATABASE_SESSIONS)
    {
        ini_set('session.use_cookies', 1);
        ini_set('session.use_only_cookies', 1);

        session_name($this->name);

        session_set_cookie_params(
            $this->cookie['lifetime'],
            $this->cookie['path'],
            $this->cookie['domain'],
            $this->cookie['secure'],
            $this->cookie['httponly']
        );

        if ($db_session === true) {
            session_set_save_handler(
                [$this, "_open"],
                [$this, "_close"],
                [$this, "_read"],
                [$this, "_write"],
                [$this, "_destroy"],
                [$this, "_gc"]
            );
        }
    }

    /**
     * @return bool
     *
     * Start the session and generate an id
     */
    public function start()
    {
        if (session_id() === '') {
            if (session_start()) {
                setcookie(
                    $this->name,
                    session_id(),
                    time() + $this->cookie['lifetime'],
                    $this->cookie['path'],
                    $this->cookie['domain'],
                    $this->cookie['secure'],
                    $this->cookie['httponly']
                );

                return 0;
            }
        }
        return false;
    }

    /**
     * @param $session_name
     *
     * Delete one individual session and reset its value to nothing
     */
    public function delete($session_name)
    {
        $this->set($session_name, '');

        return;
    }

    /**
     * @return bool
     *
     * Destroy the session and set the cookie
     */
    public function end()
    {
        if (session_id() === '') {
            return false;
        }

        $_SESSION = [];

        unset($_COOKIE[$this->name]);

        setcookie(
            $this->name,
            '',
            time() - 43200,
            $this->cookie['path'],
            $this->cookie['domain'],
            $this->cookie['secure'],
            $this->cookie['httponly']
        );

        return session_destroy();
    }

    /**
     * @param string $session_id
     * @return string
     *
     * Decrypts the data being read for the session
     */
    public function read($session_id)
    {
        $data = explode(':', parent::read($session_id));
        list($iv, $session_data) = $data;

        return openssl_decrypt($session_data, $this->openssl_method, $this->key, 0, base64_decode($iv));
    }

    /**
     * @param string $session_id
     * @param string $data
     * @return bool
     *
     * Encrypts the data being written for the session
     */
    public function write($session_id, $data)
    {
        $ivsize         = openssl_cipher_iv_length($this->openssl_method);
        $iv             = openssl_random_pseudo_bytes($ivsize);
        $encrypted_data = openssl_encrypt($data, $this->openssl_method, $this->key, 0, $iv);
        $session_data   = base64_encode($iv) . ':' . $encrypted_data;

        return parent::write($session_id, $session_data);
    }

    /**
     * @return bool
     *
     * Check to see if the session is still active, requires activity every $ttl minutes
     */
    public function isActive()
    {
        $last = isset($_SESSION['_last_activity']) ? $_SESSION['_last_activity'] : false;

        if ($last !== false && time() - $last > $this->ttl * 60) {
            return false;
        }

        $_SESSION['_last_activity'] = time();

        return true;
    }

    /**
     * @return bool
     *
     * Protect against XSS by verifying the person using the site is the one who set up the session
     */
    public function isIdentified()
    {
        $hash = md5($_SERVER['HTTP_USER_AGENT'] . (inet_ntop(inet_pton($_SERVER['REMOTE_ADDR'])) & inet_ntop(inet_pton('255.255.0.0'))));

        if (isset($_SESSION['_identified'])) {
            return $_SESSION['_identified'] === $hash;
        }

        $_SESSION['_identified'] = $hash;

        return true;
    }

    /**
     * @return bool
     *
     * Check to see if the session is active and the user is identified
     */
    public function isValid()
    {
        return $this->isActive() && $this->isIdentified();
    }

    public function getId()
    {
        return session_id();
    }

    public function refresh()
    {
        return session_regenerate_id(true);
    }

    /**
     * @param $name
     * @param $value
     *
     * Set session variables
     * $session->set('name', 'value')
     */
    public function set($name, $value)
    {
        if (session_id() === '') {
            $this->start();
        }

        $parsed = explode('.', $name);

        $session =& $_SESSION;

        while (count($parsed) > 1) {
            $next = array_shift($parsed);

            if (!isset($session[$next]) || !is_array($session[$next])) {
                $session[$next] = [];
            }

            $session =& $session[$next];
        }

        if (is_object(json_decode($value))) {
            $session[array_shift($parsed)] = json_decode($value, true);
            return;
        }

        $session[array_shift($parsed)] = $value;

        return $value;
    }

    /**
     * @param $name
     * @return null
     *
     * Get and return the sessino variables
     * $session->get('name');
     */
    public function get($name)
    {
        if (session_id() === '') {
            $this->start();
        }

        $parsed = explode('.', $name);
        $result = $_SESSION;

        while ($parsed) {
            $next = array_shift($parsed);

            if (isset($result[$next])) {
                $result = $result[$next];
            } else {
                return null;
            }
        }

        return $result;
    }

    /**
     * Open the database connection for sessions
     */
    public function _open()
    {
        if (isset($this->db)) {
            return true;
        }

        return false;
    }

    /**
     * Close the database connection
     */
    public function _close()
    {
        if ($this->db = null) {
            return true;
        }

        return false;
    }

    /**
     * @param $id
     * @return string
     *
     * Read the session information from the database
     */
    public function _read($id)
    {
        $query = 'SELECT data FROM session_storage WHERE id = :id';
        $stmt  = $this->db->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $res   = $stmt->fetch(PDO::FETCH_ASSOC);

        if (isset($res) && ! empty($res)) {
            $data = explode(':', $res['data']);
            list($iv, $session_data) = $data;

            return openssl_decrypt($session_data, $this->openssl_method, $this->key, 0, base64_decode($iv));
        } else {
            return '';
        }
    }

    /**
     * @param $id
     * @param $data
     * @return bool
     *
     * Write the session data to the database
     */
    public function _write($id, $data)
    {
        $access = time();

        $ivsize         = openssl_cipher_iv_length($this->openssl_method);
        $iv             = openssl_random_pseudo_bytes($ivsize);
        $encrypted_data = openssl_encrypt($data, $this->openssl_method, $this->key, 0, $iv);

        $session_data   = base64_encode($iv) . ':' . $encrypted_data;

        $query = 'REPLACE INTO session_storage VALUES (:id, :access, :data)';
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':access', $access);
        $stmt->bindParam(':data', $session_data);
        $stmt->execute();

        $session = $stmt->rowCount();

        if (isset($session)) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return bool
     *
     * Destroy the database session
     */
    public function _destroy($id)
    {
        $query = 'DELETE FROM session_storage WHERE id = :id';
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        $res = $stmt->rowCount();

        if (isset($res)) {
            return true;
        }
        return false;
    }

    /**
     * @param $max
     * @return bool
     *
     * Get rid of the garbage / old sessions from the database
     */
    public function _gc($max)
    {
        $old = time() - $max;

        $query = 'DELETE * FROM session_storage WHERE access < :old';
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':old', $old);
        $stmt->execute();

        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        if (isset($res)) {
            return true;
        }
        return false;
    }
}